package com.rizkyagungp.clicpay.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rizkyagungp.clicpay.dto.CustomsPage;
import com.rizkyagungp.clicpay.dto.OverbookingRequest;
import com.rizkyagungp.clicpay.dto.Transfer;
import com.rizkyagungp.clicpay.model.TCkCpTxn;
import com.rizkyagungp.clicpay.service.TransferService;
import com.rizkyagungp.clicpay.utils.ServiceError;
import com.rizkyagungp.clicpay.utils.ServiceStatus;
import com.rizkyagungp.clicpay.utils.ServiceStatus.STATUS;

import lombok.extern.slf4j.Slf4j;

@RequestMapping(value = "/api/v1/clicpay/transfer")
@CrossOrigin
@RestController
@Slf4j
public class TransferController {

    @Autowired
    private TransferService transferService;

    /* @RequestMapping(value = "/session", method = RequestMethod.POST)
	public ResponseEntity<Object> createSession(@RequestBody JwtRequest jwt, HttpServletRequest request, HttpServletResponse response) {
        log.debug("createSession");
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
            long now = Calendar.getInstance().getTimeInMillis() * JWT_TOKEN_VALIDITY;
            JwtUser jwtUser = this.jwtUtil.generateJwtUser(jwt.getUsername(), jwt.getPassword(), now);
            serviceStatus.setStatus(STATUS.COMPLETED);
			serviceStatus.setData(this.jwtUtil.generateToken(jwtUser));
			return new ResponseEntity<Object>(serviceStatus, HttpStatus.OK);
        } catch (Exception e) {
            log.error("createSession", e);
			serviceStatus.setStatus(STATUS.EXCEPTION);
			serviceStatus.setErr(new ServiceError(-400, e));
			return new ResponseEntity<Object>(serviceStatus, HttpStatus.BAD_REQUEST);
        }
    } */

    @RequestMapping(value = "/statement/overbooking", method = RequestMethod.POST)
    public ResponseEntity<Object> statementOverbooking(@RequestBody OverbookingRequest request, Map<String, String> headers) {
        log.debug("statementOverbooking");
        ServiceStatus serviceStatus = new ServiceStatus();
        try {
            TCkCpTxn transaction = new TCkCpTxn();
            transaction.setTxnReq("/api/v1/clicpay/transfer/overbooking");
            serviceStatus.setStatus(STATUS.COMPLETED);
            serviceStatus = transferService.transferOverbooking(transaction, request);
            return new ResponseEntity<Object>(serviceStatus, HttpStatus.OK);
        } catch (Exception e) {
            log.error("statementOverbooking", e);
			serviceStatus.setStatus(STATUS.EXCEPTION);
			serviceStatus.setErr(new ServiceError(-400, e.getMessage()));
			return new ResponseEntity<Object>(serviceStatus, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "list/{start}/{length}", method = RequestMethod.POST)
    public ResponseEntity<Object> getListTransfer(@PathVariable int start, @PathVariable int length, @RequestBody(required = false) Transfer transfer) {
        log.debug("getListTransfer");
        ServiceStatus serviceStatus = new ServiceStatus();
        CustomsPage<Transfer> transferPage = transferService.filterTransfer(transfer, start, length);
        serviceStatus.setData(transferPage);
        return new ResponseEntity<Object>(serviceStatus, HttpStatus.OK);
    }
}
