package com.rizkyagungp.clicpay.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.rizkyagungp.clicpay.utils.AbstractEntities;

@Entity
@Table(name = "T_CK_CP_TRANSFER")
public class TCkCpTransfer extends AbstractEntities<TCkCpTransfer> {

    private String tfrId;
    private TCkCpTxn TCkCpTxn;
    private TCkCpMstTxnType TCkCpMstTxnType;
    private String tfrReference;
    private String tfrSenderAccountNumber;
    private String tfrSenderAccountName;
    private String tfrBeneficiaryBank;
    private String tfrBeneficiaryAccountNumber;
    private String tfrBeneficiaryAccountName;
    private BigDecimal tfrBillAmount;
    private BigDecimal tfrPayAmount;
    private String tfrCcy;
    private String tfrPaymentReference;
    private String tfrDescription;
    private Character tfrStatus;
    private Date tfrDtCreate;
    private String tfrUidCreate;
    private Date tfrDtLupd;
    private String tfrUidLupd;

    public TCkCpTransfer() {
    }

    public TCkCpTransfer(String tfrId, TCkCpTxn TCkCpTxn, TCkCpMstTxnType TCkCpMstTxnType, String tfrReference, String tfrSenderAccountNumber, String tfrSenderAccountName, String tfrBeneficiaryBank, String tfrBeneficiaryAccountNumber, String tfrBeneficiaryAccountName, BigDecimal tfrBillAmount, BigDecimal tfrPayAmount, String tfrCcy, String tfrPaymentReference, String tfrDescription, Character tfrStatus, Date tfrDtCreate, String tfrUidCreate, Date tfrDtLupd, String tfrUidLupd) {
        this.tfrId = tfrId;
        this.TCkCpTxn = TCkCpTxn;
        this.TCkCpMstTxnType = TCkCpMstTxnType;
        this.tfrReference = tfrReference;
        this.tfrSenderAccountNumber = tfrSenderAccountNumber;
        this.tfrSenderAccountName = tfrSenderAccountName;
        this.tfrBeneficiaryBank = tfrBeneficiaryBank;
        this.tfrBeneficiaryAccountNumber = tfrBeneficiaryAccountNumber;
        this.tfrBeneficiaryAccountName = tfrBeneficiaryAccountName;
        this.tfrBillAmount = tfrBillAmount;
        this.tfrPayAmount = tfrPayAmount;
        this.tfrCcy = tfrCcy;
        this.tfrPaymentReference = tfrPaymentReference;
        this.tfrDescription = tfrDescription;
        this.tfrStatus = tfrStatus;
        this.tfrDtCreate = tfrDtCreate;
        this.tfrUidCreate = tfrUidCreate;
        this.tfrDtLupd = tfrDtLupd;
        this.tfrUidLupd = tfrUidLupd;
    }

    @Id
    @Column(name = "TFR_ID", unique = true, nullable = false, length = 35)
    public String getTfrId() {
        return this.tfrId;
    }

    public void setTfrId(String tfrId) {
        this.tfrId = tfrId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TFR_TXN_ID", nullable = false)
    public TCkCpTxn getTCkCpTxn() {
        return this.TCkCpTxn;
    }

    public void setTCkCpTxn(TCkCpTxn TCkCpTxn) {
        this.TCkCpTxn = TCkCpTxn;
    }

    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TFR_TYPE_ID", nullable = false)
    public TCkCpMstTxnType getTCkCpMstTxnType() {
        return this.TCkCpMstTxnType;
    }

    public void setTCkCpMstTxnType(TCkCpMstTxnType TCkCpMstTxnType) {
        this.TCkCpMstTxnType = TCkCpMstTxnType;
    }

    @Column(name = "TFR_REFERENCE", length = 100)
    public String getTfrReference() {
        return this.tfrReference;
    }

    public void setTfrReference(String tfrReference) {
        this.tfrReference = tfrReference;
    }

    @Column(name = "TFR_SENDER_ACCOUNT_NUMBER", length = 35)
    public String getTfrSenderAccountNumber() {
        return this.tfrSenderAccountNumber;
    }

    public void setTfrSenderAccountNumber(String tfrSenderAccountNumber) {
        this.tfrSenderAccountNumber = tfrSenderAccountNumber;
    }

    @Column(name = "TFR_SENDER_ACCOUNT_NAME")
    public String getTfrSenderAccountName() {
        return this.tfrSenderAccountName;
    }

    public void setTfrSenderAccountName(String tfrSenderAccountName) {
        this.tfrSenderAccountName = tfrSenderAccountName;
    }

    @Column(name = "TFR_BENEFICIARY_BANK", length = 35)
    public String getTfrBeneficiaryBank() {
        return this.tfrBeneficiaryBank;
    }

    public void setTfrBeneficiaryBank(String tfrBeneficiaryBank) {
        this.tfrBeneficiaryBank = tfrBeneficiaryBank;
    }

    @Column(name = "TFR_BENEFICIARY_ACCOUNT_NUMBER", length = 35)
    public String getTfrBeneficiaryAccountNumber() {
        return this.tfrBeneficiaryAccountNumber;
    }

    public void setTfrBeneficiaryAccountNumber(String tfrBeneficiaryAccountNumber) {
        this.tfrBeneficiaryAccountNumber = tfrBeneficiaryAccountNumber;
    }

    @Column(name = "TFR_BENEFICIARY_ACCOUNT_NAME")
    public String getTfrBeneficiaryAccountName() {
        return this.tfrBeneficiaryAccountName;
    }

    public void setTfrBeneficiaryAccountName(String tfrBeneficiaryAccountName) {
        this.tfrBeneficiaryAccountName = tfrBeneficiaryAccountName;
    }

    @Column(name = "TFR_BILL_AMOUNT", precision = 15, scale = 2)
    public BigDecimal getTfrBillAmount() {
        return this.tfrBillAmount;
    }

    public void setTfrBillAmount(BigDecimal tfrBillAmount) {
        this.tfrBillAmount = tfrBillAmount;
    }

    @Column(name = "TFR_PAY_AMOUNT", precision = 15, scale = 2)
    public BigDecimal getTfrPayAmount() {
        return this.tfrPayAmount;
    }

    public void setTfrPayAmount(BigDecimal tfrPayAmount) {
        this.tfrPayAmount = tfrPayAmount;
    }

    @Column(name = "TFR_CCY", length = 35)
    public String getTfrCcy() {
        return this.tfrCcy;
    }

    public void setTfrCcy(String tfrCcy) {
        this.tfrCcy = tfrCcy;
    }

    @Column(name = "TFR_PAYMENT_REFERENCE")
    public String getTfrPaymentReference() {
        return this.tfrPaymentReference;
    }

    public void setTfrPaymentReference(String tfrPaymentReference) {
        this.tfrPaymentReference = tfrPaymentReference;
    }

    @Column(name = "TFR_DESCRIPTION", length = 512)
    public String getTfrDescription() {
        return this.tfrDescription;
    }

    public void setTfrDescription(String tfrDescription) {
        this.tfrDescription = tfrDescription;
    }

    @Column(name = "TFR_STATUS", length = 1)
    public Character getTfrStatus() {
        return this.tfrStatus;
    }

    public void setTfrStatus(Character tfrStatus) {
        this.tfrStatus = tfrStatus;
    }

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TFR_DT_CREATE", length = 19)
    public Date getTfrDtCreate() {
        return this.tfrDtCreate;
    }

    public void setTfrDtCreate(Date tfrDtCreate) {
        this.tfrDtCreate = tfrDtCreate;
    }

    @Column(name = "TFR_UID_CREATE", length = 35)
    public String getTfrUidCreate() {
        return this.tfrUidCreate;
    }

    public void setTfrUidCreate(String tfrUidCreate) {
        this.tfrUidCreate = tfrUidCreate;
    }

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TFR_DT_LUPD", length = 19)
    public Date getTfrDtLupd() {
        return this.tfrDtLupd;
    }

    public void setTfrDtLupd(Date tfrDtLupd) {
        this.tfrDtLupd = tfrDtLupd;
    }

    @Column(name = "TFR_UID_LUPD", length = 35)
    public String getTfrUidLupd() {
        return this.tfrUidLupd;
    }

    public void setTfrUidLupd(String tfrUidLupd) {
        this.tfrUidLupd = tfrUidLupd;
    }

    @Override
    public int compareTo(TCkCpTransfer o) {
        return 0;
    }

    @Override
    public void init() {
    }
    
}
