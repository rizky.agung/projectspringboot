package com.rizkyagungp.clicpay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClicpayApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClicpayApplication.class, args);
	}

}
