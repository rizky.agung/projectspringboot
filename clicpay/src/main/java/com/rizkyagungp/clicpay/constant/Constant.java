package com.rizkyagungp.clicpay.constant;

public class Constant {
    
    public static final char ACTIVE_STATUS = 'A';
    
    public static final char INACTIVE_STATUS = 'I';

    public static final char QUEUE_STATUS = 'Q';

    public static final char INQUIRY_STATUS = 'I';

    public static final char FAILED_STATUS = 'F';

    public static final char PAYMENT_STATUS = 'P';
}
