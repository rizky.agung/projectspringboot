package com.rizkyagungp.clicpay.utils;

import org.apache.commons.lang3.exception.ExceptionUtils;

public class ServiceError extends AbstractEntities<ServiceError> {

    private int code;
  
    private String msg;

    private String stack;

    public ServiceError() {}

    public ServiceError(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public ServiceError(int code, Exception ex) {
        this.code = code;
        this.msg = ex.getMessage();
        this.stack = ExceptionUtils.getStackTrace(ex);
    }

    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStack() {
        return this.stack;
    }

    public void setStack(String stack) {
        this.stack = stack;
    }

    @Override
    public int compareTo(ServiceError o) {
        return 0;
    }

    @Override
    public void init() {
    }
    
}
