package com.rizkyagungp.clicpay.utils;

public class ServiceStatus extends AbstractEntities<ServiceStatus> {
    
    private STATUS status;
  
    private Object data;

    private ServiceError err;

    public enum STATUS {
        NEW, ABORT, COMPLETED, FAILED, SUCCESS, EXCEPTION, ERROR, VALIDATION_FAILED, PERMISSION_FAILED;
    }

    public STATUS getStatus() {
        return this.status;
    }

    public void setStatus(STATUS status) {
        this.status = status;
    }

    public Object getData() {
        return this.data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public ServiceError getErr() {
        return this.err;
    }

    public void setErr(ServiceError err) {
        this.err = err;
    }

    @Override
    public int compareTo(ServiceStatus o) {
        return 0;
    }

    @Override
    public void init() {
        this.status = STATUS.NEW;
    }
}
