package com.rizkyagungp.clicpay.service.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.rizkyagungp.clicpay.constant.Constant;
import com.rizkyagungp.clicpay.dao.CkCpTransferListDao;
import com.rizkyagungp.clicpay.dto.CustomsPage;
import com.rizkyagungp.clicpay.dto.OverbookingRequest;
import com.rizkyagungp.clicpay.dto.Transfer;
import com.rizkyagungp.clicpay.dto.client.ReqInquiryOverbooking;
import com.rizkyagungp.clicpay.dto.client.ReqPaymentOverbooking;
import com.rizkyagungp.clicpay.dto.client.ResInquiryOverbooking;
import com.rizkyagungp.clicpay.model.TCkCpTransfer;
import com.rizkyagungp.clicpay.model.TCkCpTxn;
import com.rizkyagungp.clicpay.service.AbstractTransferService;
import com.rizkyagungp.clicpay.service.IClientService;
import com.rizkyagungp.clicpay.utils.CpUtil;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TransferServiceImpl extends AbstractTransferService {

    @Autowired
    private IClientService iClientService;

    @Autowired
    private CkCpTransferListDao ckCpTransferListDao;
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
    protected TCkCpTransfer doTransferOverbooking(TCkCpTxn transaction, OverbookingRequest request) throws Exception {
        log.debug("doTransferOverbooking");
        try {
            Date now = Calendar.getInstance().getTime();
            if (null == transaction)
                throw new Exception("param tCkCpTxn null");

            if (null == request)
                throw new Exception("param request null");

            TCkCpTransfer transfer = new TCkCpTransfer();
            transfer.setTfrId(CpUtil.generateId("TFR"));
            transfer.setTCkCpTxn(transaction);
            transfer.setTCkCpMstTxnType(transaction.getTCkCpMstTxnType());
            transfer.setTfrReference(transaction.getTxnReference());
            transfer.setTfrSenderAccountNumber(request.getSenderAccount());
            transfer.setTfrBillAmount(request.getAmount());
            transfer.setTfrBeneficiaryAccountNumber(request.getBeneficiaryAccount());
            transfer.setTfrCcy(request.getCcy());
            transfer.setTfrStatus(Constant.QUEUE_STATUS);
            transfer.setTfrDtCreate(now);
            transfer.setTfrUidCreate(SYSTEM);
            transfer.setTfrDtLupd(now);
            transfer.setTfrUidLupd(SYSTEM);
            ckCpTransferDao.save(transfer);

            TCkCpTransfer inquiry = this.inquiryOverbooking(transaction, transfer);
            TCkCpTransfer payment = this.paymentOverbooking(transaction, inquiry);
            return payment;
        } catch (Exception e) {
            throw e;
        }
    }

    private TCkCpTransfer inquiryOverbooking(TCkCpTxn transaction, TCkCpTransfer transfer) throws Exception {
        log.debug("inquiryOverbooking");
        try {
            ReqInquiryOverbooking request = new ReqInquiryOverbooking();
            request.setUserReferenceNumber(generetedUsrRerNo());
            request.setAccountNumber(transfer.getTfrBeneficiaryAccountNumber());

            ResInquiryOverbooking response =  iClientService.inquiryOverbooking(transaction, request);
            transfer.setTfrBeneficiaryAccountName(response.getAccountName());
            transfer.setTfrStatus(Constant.INQUIRY_STATUS);
            ckCpTransferDao.save(transfer);
            return transfer;
        } catch (Exception e) {
            transfer.setTfrStatus(Constant.FAILED_STATUS);
            ckCpTransferDao.save(transfer);

            throw e;
        }
    }

    private TCkCpTransfer paymentOverbooking(TCkCpTxn transaction, TCkCpTransfer transfer) throws Exception {
        log.debug("paymentOverbooking");
        try {
            ReqPaymentOverbooking request = new ReqPaymentOverbooking();
            request.setUserReferenceNumber(generetedUsrRerNo());
            request.setSourceAccountNumber(transfer.getTfrSenderAccountNumber());
            request.setBeneficiaryAccountNumber(transfer.getTfrBeneficiaryAccountNumber());
            request.setBeneficiaryName(transfer.getTfrBeneficiaryAccountName());
            request.setAmount(transfer.getTfrBillAmount().setScale(2).toString());
            request.setDescription("PAYMENT OvERBOOKING");
            request.setTransactionDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));

            iClientService.paymentOverbooking(transaction, request);
            transfer.setTfrStatus(Constant.PAYMENT_STATUS);
            transfer.setTfrPayAmount(new BigDecimal(request.getAmount()));
            transfer.setTfrDescription(request.getDescription());
            ckCpTransferDao.save(transfer);

            return transfer;
        } catch (Exception e) {
            transfer.setTfrStatus(Constant.FAILED_STATUS);
            ckCpTransferDao.save(transfer);
            throw e;
        }
    }

    private String generetedUsrRerNo() {
        log.debug("generateUsrRefNo");
        return String.format("%s%s", "9100", String.valueOf(System.currentTimeMillis()));
    }

    @Override
    @Transactional(readOnly = true)
    public CustomsPage<Transfer> filterTransfer(Transfer transfer, int start, int length) {
        log.debug("filterTransfer");
        long totalRow = ckCpTransferListDao.countByField(null);
        long totalFiltered = ckCpTransferListDao.countByField(transfer);
        CustomsPage<Transfer> customsPage = new CustomsPage<>(totalRow, totalFiltered);
        for (Transfer tfr : ckCpTransferListDao.findByFieldPaging(transfer, start, length)) {
            customsPage.getContent().add(tfr);
        }
        return customsPage;
    }

}
