package com.rizkyagungp.clicpay.service;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.rizkyagungp.clicpay.constant.Constant;
import com.rizkyagungp.clicpay.dao.CkCpMstTxnStateDao;
import com.rizkyagungp.clicpay.dao.CkCpMstTxnTypeDao;
import com.rizkyagungp.clicpay.dao.CkCpTransferDao;
import com.rizkyagungp.clicpay.dao.CkCpTxnDao;
import com.rizkyagungp.clicpay.dto.AbstractRequest;
import com.rizkyagungp.clicpay.dto.OverbookingRequest;
import com.rizkyagungp.clicpay.enums.TxnState;
import com.rizkyagungp.clicpay.enums.TxnType;
import com.rizkyagungp.clicpay.model.TCkCpMstTxnState;
import com.rizkyagungp.clicpay.model.TCkCpMstTxnType;
import com.rizkyagungp.clicpay.model.TCkCpTransfer;
import com.rizkyagungp.clicpay.model.TCkCpTxn;
import com.rizkyagungp.clicpay.utils.CpUtil;
import com.rizkyagungp.clicpay.utils.ServiceStatus;
import com.rizkyagungp.clicpay.utils.ServiceStatus.STATUS;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractTransferService implements TransferService {

    protected static final String SYSTEM = "SYS";

    @Autowired
    protected CkCpMstTxnTypeDao ckCpMstTxnTypeDao;

    @Autowired
    protected CkCpMstTxnStateDao ckCpMstTxnStateDao;

    @Autowired
    protected CkCpTxnDao ckCpTxnDao;

    @Autowired
    protected CkCpTransferDao ckCpTransferDao;

    protected abstract TCkCpTransfer doTransferOverbooking(TCkCpTxn transaction, OverbookingRequest request) throws Exception;

    @Override
    public ServiceStatus transferOverbooking(TCkCpTxn transaction, OverbookingRequest request) throws Exception {
        log.debug("transferOverbooking");
        try {
            ServiceStatus serviceStatus = new ServiceStatus();
            Date now = Calendar.getInstance().getTime();

            Optional<TCkCpMstTxnType> tCkCpMstTxnType = ckCpMstTxnTypeDao.findById(TxnType.TRANSFER.getId());
            if (!tCkCpMstTxnType.isPresent())
                throw new Exception("param tCkCpMstTxnType not found : " + TxnType.TRANSFER.getId());

            long countTCkCpTransfer = ckCpTransferDao.countByFieldBeneficiaryAccountAndStatus(request.getBeneficiaryAccount(), Arrays.asList(Constant.QUEUE_STATUS, Constant.INQUIRY_STATUS));
            log.info("countTCkCpTransfer" + countTCkCpTransfer);
            if (countTCkCpTransfer > 0)
                throw new Exception("complete the payment beforehand");

            TCkCpTxn tCkCpTxn = this.createTxn(request, tCkCpMstTxnType.get(), now);
            tCkCpTxn.setTxnDtReq(now);
            tCkCpTxn.setTxnReq(transaction.getTxnReq());
            tCkCpTxn.setTxnParam(request.toJson());
            ckCpTxnDao.save(tCkCpTxn);

            serviceStatus.setStatus(STATUS.COMPLETED);
            serviceStatus.setData(doTransferOverbooking(tCkCpTxn, request));

            return serviceStatus;
        } catch (Exception e) {
            throw e;
        }
    }
    
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
    protected TCkCpTxn createTxn(AbstractRequest request, TCkCpMstTxnType txnType, Date now) throws Exception {
        log.debug("createTxn");
        if (null == request)
            throw new Exception("param request null");

        if (null == txnType)
            throw new Exception("param tCkCpMstTxnType null");

        TCkCpTxn transaction = new TCkCpTxn();
        transaction.setTxnId(CpUtil.generateId());
        transaction.setTCkCpMstTxnType(txnType);

        Optional<TCkCpMstTxnState> tCkCpMstTxnState = ckCpMstTxnStateDao.findById(TxnState.NEW.getId());
        if (!tCkCpMstTxnState.isPresent())
            throw new Exception("param tCkCpMstTxnState not found : " + TxnState.NEW.getId());

        transaction.setTCkCpMstTxnState(tCkCpMstTxnState.get());
        transaction.setTxnReference(request.getReffNumber());
        transaction.setTxnStatus(Constant.ACTIVE_STATUS);
        transaction.setTxnDtCreate(now);
        transaction.setTxnUidCreate(SYSTEM);
        transaction.setTxnDtLupd(now);
        transaction.setTxnUidLupd(SYSTEM);
        return transaction;
    }
}
