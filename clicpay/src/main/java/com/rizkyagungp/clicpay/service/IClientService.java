package com.rizkyagungp.clicpay.service;

import com.rizkyagungp.clicpay.dto.client.ReqInquiryOverbooking;
import com.rizkyagungp.clicpay.dto.client.ReqPaymentOverbooking;
import com.rizkyagungp.clicpay.dto.client.ResInquiryOverbooking;
import com.rizkyagungp.clicpay.dto.client.ResPaymentOverbooking;
import com.rizkyagungp.clicpay.model.TCkCpTxn;

public interface IClientService {
    
    public ResInquiryOverbooking inquiryOverbooking(TCkCpTxn transaction, ReqInquiryOverbooking request) throws Exception;

    public ResPaymentOverbooking paymentOverbooking(TCkCpTxn tranaction, ReqPaymentOverbooking request) throws Exception;
}
