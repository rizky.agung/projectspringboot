package com.rizkyagungp.clicpay.service.impl;

import java.time.Instant;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.rizkyagungp.clicpay.dao.CkCpTxnDao;
import com.rizkyagungp.clicpay.dto.client.ReqInquiryOverbooking;
import com.rizkyagungp.clicpay.dto.client.ReqPaymentOverbooking;
import com.rizkyagungp.clicpay.dto.client.ResInquiryOverbooking;
import com.rizkyagungp.clicpay.dto.client.ResPaymentOverbooking;
import com.rizkyagungp.clicpay.model.TCkCpTxn;
import com.rizkyagungp.clicpay.service.IClientService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ClientServiceImpl implements IClientService {
    
    private static String HOST_URL = "http://springboot-mysql-servpay:8002/api/v1/server";

    @Autowired
    private CkCpTxnDao ckCpTxnDao;
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
    public ResInquiryOverbooking inquiryOverbooking(TCkCpTxn transaction, ReqInquiryOverbooking request) throws Exception {
        log.debug("inquiryOverbooking");
        Date now = Calendar.getInstance().getTime();
        try {
            if (null == transaction)
                throw new Exception("param tCkCpTxn null");

            if (null == request)
                throw new Exception("param request null");

            String BDI_TIMESTAMP = Instant.now().toString();

            HttpHeaders headers = new HttpHeaders();
            headers.set("BDI-timestamp", BDI_TIMESTAMP);

            RestTemplate restTemplate = new RestTemplate();
            HttpEntity<ReqInquiryOverbooking> payload = new HttpEntity<>(request, headers);
            ResponseEntity<ResInquiryOverbooking> response = restTemplate.exchange(HOST_URL + "/overbooking/inquiry", HttpMethod.POST, payload, ResInquiryOverbooking.class);

            transaction.setTxnResp(response.getBody().toJson());
            transaction.setTxnRespCode((short) 200);
            transaction.setTxnDtResp(now);
            transaction.setTxnDtLupd(now);
            ckCpTxnDao.save(transaction);

            return response.getBody();
        } catch (Exception e) {
            transaction.setTxnResp(e.getMessage());
            transaction.setTxnRespCode((short) 400);
            transaction.setTxnDtResp(now);
            transaction.setTxnDtLupd(now);
            ckCpTxnDao.save(transaction);

            throw e;
        }
    }

    @Override
    public ResPaymentOverbooking paymentOverbooking(TCkCpTxn transaction, ReqPaymentOverbooking request)
            throws Exception {
        log.debug("paymentOverbooking");
        Date now = Calendar.getInstance().getTime();
        try {
            if (null == transaction)
                throw new Exception("param tCkCpTxn null");

            if (null == request)
                throw new Exception("param request null");

            String BDI_TIMESTAMP = Instant.now().toString();

            HttpHeaders headers = new HttpHeaders();
            headers.set("BDI-timestamp", BDI_TIMESTAMP);

            RestTemplate restTemplate = new RestTemplate();
            HttpEntity<ReqPaymentOverbooking> payload = new HttpEntity<>(request, headers);
            ResponseEntity<ResPaymentOverbooking> response = restTemplate.exchange(HOST_URL + "/overbooking/payment", HttpMethod.POST, payload, ResPaymentOverbooking.class);
            
            transaction.setTxnResp(response.getBody().toJson());
            transaction.setTxnRespCode((short) 200);
            transaction.setTxnDtResp(now);
            transaction.setTxnDtLupd(now);
            ckCpTxnDao.save(transaction);

            return response.getBody();
        } catch (Exception e) {
            transaction.setTxnResp(e.getMessage());
            transaction.setTxnRespCode((short) 400);
            transaction.setTxnDtResp(now);
            transaction.setTxnDtLupd(now);
            ckCpTxnDao.save(transaction);
            
            throw e;
        }
    }
    
}
