package com.rizkyagungp.clicpay.service;

import com.rizkyagungp.clicpay.dto.CustomsPage;
import com.rizkyagungp.clicpay.dto.OverbookingRequest;
import com.rizkyagungp.clicpay.dto.Transfer;
import com.rizkyagungp.clicpay.model.TCkCpTxn;
import com.rizkyagungp.clicpay.utils.ServiceStatus;

public interface TransferService {
    
    // String generateSession(String uid, long exp) throws Exception;

    ServiceStatus transferOverbooking(TCkCpTxn transaction, OverbookingRequest request) throws Exception;

    CustomsPage<Transfer> filterTransfer(Transfer transfer, int start, int length);
}
