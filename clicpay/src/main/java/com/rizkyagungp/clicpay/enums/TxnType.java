package com.rizkyagungp.clicpay.enums;

public enum TxnType {
    
    LOGIN("LOGIN"), TRANSFER("TRANSFER");

	private String id;

	private TxnType(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}
}
