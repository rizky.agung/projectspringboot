package com.rizkyagungp.clicpay.enums;

public enum TxnState {
    
    NEW("NEW"), SUCCESS("SUCCESS"), EXCEPTION("EXCEPTION"), FAILED("FAILED");

	private String id;

	private TxnState(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}
}
