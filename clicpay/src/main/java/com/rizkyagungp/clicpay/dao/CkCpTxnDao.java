package com.rizkyagungp.clicpay.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rizkyagungp.clicpay.model.TCkCpTxn;

@Repository
public interface CkCpTxnDao extends JpaRepository<TCkCpTxn, String> {
    
}
