package com.rizkyagungp.clicpay.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rizkyagungp.clicpay.model.TCkCpMstTxnState;

@Repository
public interface CkCpMstTxnStateDao extends JpaRepository<TCkCpMstTxnState, String> {
    
}
