package com.rizkyagungp.clicpay.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rizkyagungp.clicpay.model.TCkCpMstTxnType;

@Repository
public interface CkCpMstTxnTypeDao extends JpaRepository<TCkCpMstTxnType, String> {
    
}
