package com.rizkyagungp.clicpay.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.rizkyagungp.clicpay.model.TCkCpTransfer;

@Repository
public interface CkCpTransferDao extends JpaRepository<TCkCpTransfer, String>{
    
    @Query(value = "SELECT count(t.TFR_ID) FROM t_ck_cp_transfer t "
                    + "WHERE t.TFR_BENEFICIARY_ACCOUNT_NUMBER = :beneficiaryAccountNumber AND t.TFR_STATUS IN :tfrStatus", nativeQuery = true)
    long countByFieldBeneficiaryAccountAndStatus(String beneficiaryAccountNumber, List<Character> tfrStatus);

}
