package com.rizkyagungp.clicpay.dao;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.rizkyagungp.clicpay.dto.Transfer;

@Repository
public class CkCpTransferListDao {
    
    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    public List<Transfer> findByFieldPaging(Transfer transfer, int start, int length) {
        if (null != transfer)
            transfer = new Transfer();
        
        String hql = "SELECT t.TFR_ID, t.TFR_REFERENCE, t.TFR_SENDER_ACCOUNT_NUMBER, t.TFR_BENEFICIARY_ACCOUNT_NUMBER, t.TFR_BENEFICIARY_ACCOUNT_NAME, " 
                    + "t.TFR_BILL_AMOUNT, t.TFR_PAY_AMOUNT, t.TFR_CCY, t.TFR_DESCRIPTION, t.TFR_STATUS, "
                    + "CASE WHEN t.TFR_STATUS = 'I' THEN 'INQUIRY' WHEN t.TFR_STATUS = 'Q' THEN 'QUEUE' WHEN t.TFR_STATUS = 'P' THEN 'PAID' "
                    + "WHEN t.TFR_STATUS = 'F' THEN 'FAILED' END AS TFR_STATUS_DESC, t.TFR_DT_CREATE AS TFR_BILL_DATE, "
                    + "IF (tx.TXN_RESP_CODE = 200, DATE_FORMAT(tx.TXN_RESP->>\"$.responseTime\", \"%Y-%m-%d %H:%I:%S\"), NULL) AS TFR_PAY_DATE "
                    + "FROM T_CK_CP_TRANSFER t "
                    + "LEFT JOIN T_CK_CP_TXN tx ON t.TFR_TXN_ID = tx.TXN_ID ";

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        hql += setConditions(parameterSource, transfer);
        hql += "ORDER BY t.TFR_DT_CREATE DESC limit :limit,:offset";
        parameterSource.addValue("limit", start);
        parameterSource.addValue("offset", length);
        List<Transfer> transfers = jdbcTemplate.query(hql.trim(), parameterSource, 
            (rs, rowNum) -> new Transfer(rs.getString("TFR_ID"), rs.getString("TFR_REFERENCE"), 
                    rs.getString("TFR_SENDER_ACCOUNT_NUMBER"), rs.getString("TFR_BENEFICIARY_ACCOUNT_NUMBER"), 
                    rs.getString("TFR_BENEFICIARY_ACCOUNT_NAME"), new BigDecimal(rs.getObject("TFR_BILL_AMOUNT").toString()), 
                    new BigDecimal(rs.getObject("TFR_PAY_AMOUNT").toString()), rs.getString("TFR_CCY"), 
                    rs.getString("TFR_DESCRIPTION"), rs.getString("TFR_STATUS").charAt(0), rs.getString("TFR_STATUS_DESC"), 
                    toDate(rs.getString("TFR_BILL_DATE")), toDate(rs.getString("TFR_BILL_DATE"))));
        return transfers;
    }

    public long countByField(Transfer transfer) {
        if (null == transfer)
            transfer = new Transfer();

        String hql = "SELECT count(t.TFR_ID) FROM T_CK_CP_TRANSFER t ";
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        hql += setConditions(parameterSource, transfer);
        Long count = jdbcTemplate.queryForObject(hql.trim(), parameterSource, Long.class);
        return Optional.ofNullable(count).orElse(0l);
    }

    private String setConditions(MapSqlParameterSource parameterSource, Transfer transfer) {
        List<String> conditions = new ArrayList<>();

        if (!Optional.ofNullable(transfer.getTfrId()).orElse("").trim().isEmpty()) {
            conditions.add("t.TFR_ID LIKE :tfrId ");
            parameterSource.addValue("tfrId", "%" + transfer.getTfrId() + "%");
        }

        if (!Optional.ofNullable(transfer.getTfrReference()).orElse("").trim().isEmpty()) {
            conditions.add("t.TFR_REFERENCE LIKE :tfrReference ");
            parameterSource.addValue("tfrReference", "%" + transfer.getTfrReference() + "%");
        }

        if (!Optional.ofNullable(transfer.getTfrSenderAccountNumber()).orElse("").trim().isEmpty()) {
            conditions.add("t.TFR_SENDER_ACCOUNT_NUMBER LIKE :tfrSenderAcountNumber ");
            parameterSource.addValue("tfrSenderAcountNumber", "%" + transfer.getTfrSenderAccountNumber() + "%");
        }

        if (!Optional.ofNullable(transfer.getTfrBeneficiaryAccountNumber()).orElse("").trim().isEmpty()) {
            conditions.add("t.TFR_BENEFICIARY_ACCOUNT_NUMBER LIKE :tfrBeneficiaryAccountNumber ");
            parameterSource.addValue("tfrBeneficiaryAccountNumber", "%" + transfer.getTfrBeneficiaryAccountNumber() + "%");
        }

        if (!Optional.ofNullable(transfer.getTfrBeneficiaryAccountName()).orElse("").trim().isEmpty()) {
            conditions.add("t.TFR_BENEFICIARY_ACCOUNT_NAME LIKE :tfrBeneficiaryAccountName ");
            parameterSource.addValue("tfrBeneficiaryAccountName", "%" + transfer.getTfrBeneficiaryAccountName() + "%");
        }

        if (null != transfer.getTfrBillAmount() && transfer.getTfrBillAmount().compareTo(BigDecimal.ZERO) > 0) {
            conditions.add("t.TFR_BILL_AMOUNT = :tfrBillAmount ");
            parameterSource.addValue("tfrBillAmount", transfer.getTfrBillAmount());
        }

        if (null != transfer.getTfrPayAmount() && transfer.getTfrPayAmount().compareTo(BigDecimal.ZERO) > 0) {
            conditions.add("t.TFR_PAY_AMOUNT = :tfrPayAmount ");
            parameterSource.addValue("tfrPayAmount", transfer.getTfrPayAmount());
        }

        if (null != transfer.getTfrPayAmount() && transfer.getTfrPayAmount().compareTo(BigDecimal.ZERO) > 0) {
            conditions.add("t.TFR_CCY = :tfrCcy ");
            parameterSource.addValue("tfrCcy", transfer.getCcy());
        }

        if (!Optional.ofNullable(transfer.getTfrDescription()).orElse("").trim().isEmpty()) {
            conditions.add("t.TFR_DESCRIPTION LIKE :tfrDescription ");
            parameterSource.addValue("tfrDescription", "%" + transfer.getTfrDescription() + "%");
        }

        if (null != transfer.getTfrStatus()) {
            conditions.add("t.TFR_STATUS = :tfrStatus ");
            parameterSource.addValue("tfrStatus", transfer.getTfrStatus());
        }

        String hql = "";
        if (!conditions.isEmpty())
            hql += "WHERE ";

        int i = 1;
        for (String condition : conditions) {
            hql += condition;
            if (i < conditions.size())
                hql += "AND ";

            i++;
        }
        return hql;
    }

    private Date toDate(String value) {
        if (null == value)
            return null;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return sdf.parse(value);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
