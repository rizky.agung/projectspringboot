package com.rizkyagungp.clicpay.dto.client;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ReqInquiryOverbooking extends AbstractClient {

    private String accountNumber;
    private String requestTime;

    public String getAccountNumber() {
        return this.accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getRequestTime() {
        return this.requestTime;
    }

    public void setRequestTime(String requestTime) {
        this.requestTime = requestTime;
    }

    @Override
    public int compareTo(AbstractClient o) {
        return 0;
    }

    @Override
    public void init() {
        Date now = Calendar.getInstance().getTime();
        this.requestTime = new SimpleDateFormat("yyyyMMddHHmmss").format(now);
    }
}
