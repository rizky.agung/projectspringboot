package com.rizkyagungp.clicpay.dto;

import java.math.BigDecimal;

public class OverbookingRequest extends AbstractRequest {

    private String senderAccount;
    private String beneficiaryAccount;
    private BigDecimal amount;
    private String ccy;

    public String getSenderAccount() {
        return this.senderAccount;
    }

    public void setSenderAccount(String senderAccount) {
        this.senderAccount = senderAccount;
    }

    public String getBeneficiaryAccount() {
        return this.beneficiaryAccount;
    }

    public void setBeneficiaryAccount(String beneficiaryAccount) {
        this.beneficiaryAccount = beneficiaryAccount;
    }

    public BigDecimal getAmount() {
        return this.amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCcy() {
        return this.ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    @Override
    public int compareTo(AbstractRequest o) {
        return 0;
    }

    @Override
    public void init() {;
    }
    
}
