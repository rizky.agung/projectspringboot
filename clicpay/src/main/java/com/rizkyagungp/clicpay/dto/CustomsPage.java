package com.rizkyagungp.clicpay.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CustomsPage<T> {
    
    private long recordsTotal;
    
    private long recordsFiltered;
    
    private List<T> content;

    public CustomsPage(long recordsTotal, long recordsFiltered) {
        this.recordsTotal = recordsTotal;
        this.recordsFiltered = recordsFiltered;
    }

    public List<T> getContent() {
        if (null == this.content)
            this.content = new ArrayList<>();

        return this.content;
    }
}
