package com.rizkyagungp.clicpay.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

import com.rizkyagungp.clicpay.utils.AbstractEntities;

public class Transfer extends AbstractEntities<Transfer> {

    private String tfrId;
    private String tfrReference;
    private String tfrSenderAccountNumber;
    private String tfrBeneficiaryAccountNumber;
    private String tfrBeneficiaryAccountName;
    private BigDecimal tfrBillAmount;
    private BigDecimal tfrPayAmount;
    private String ccy;
    private String tfrDescription;
    private Character tfrStatus;
    private String tfrStatusDesc;
    private Date tfrBillDate;
    private Date tfrPayDate;

    public Transfer() {
    }

    public Transfer(String tfrId, String tfrReference, String tfrSenderAccountNumber, String tfrBeneficiaryAccountNumber, String tfrBeneficiaryAccountName, BigDecimal tfrBillAmount, BigDecimal tfrPayAmount, String ccy, String tfrDescription, Character tfrStatus, String tfrStatusDesc, Date tfrBillDate, Date tfrPayDate) {
        this.tfrId = tfrId;
        this.tfrReference = tfrReference;
        this.tfrSenderAccountNumber = tfrSenderAccountNumber;
        this.tfrBeneficiaryAccountNumber = tfrBeneficiaryAccountNumber;
        this.tfrBeneficiaryAccountName = tfrBeneficiaryAccountName;
        this.tfrBillAmount = tfrBillAmount;
        this.tfrPayAmount = tfrPayAmount;
        this.ccy = ccy;
        this.tfrDescription = tfrDescription;
        this.tfrStatus = tfrStatus;
        this.tfrStatusDesc = tfrStatusDesc;
        this.tfrBillDate = tfrBillDate;
        this.tfrPayDate = tfrPayDate;
    }

    public String getTfrId() {
        return Optional.ofNullable(this.tfrId).orElse("");
    }

    public String getTfrReference() {
        return Optional.ofNullable(this.tfrReference).orElse("");
    }

    public String getTfrSenderAccountNumber() {
        return Optional.ofNullable(this.tfrSenderAccountNumber).orElse("");
    }

    public String getTfrBeneficiaryAccountNumber() {
        return Optional.ofNullable(this.tfrBeneficiaryAccountNumber).orElse("");
    }

    public String getTfrBeneficiaryAccountName() {
        return Optional.ofNullable(this.tfrBeneficiaryAccountName).orElse("");
    }

    public BigDecimal getTfrBillAmount() {
        return this.tfrBillAmount;
    }

    public BigDecimal getTfrPayAmount() {
        return this.tfrPayAmount;
    }

    public String getCcy() {
        return Optional.ofNullable(this.ccy).orElse("");
    }

    public String getTfrDescription() {
        return Optional.ofNullable(this.tfrDescription).orElse("");
    }

    public Character getTfrStatus() {
        return this.tfrStatus;
    }

    public String getTfrStatusDesc() {
        return Optional.ofNullable(this.tfrStatusDesc).orElse("");
    }

    public Date getTfrBillDate() {
        return this.tfrBillDate;
    }

    public Date getTfrPayDate() {
        return this.tfrPayDate;
    }

    
    @Override
    public int compareTo(Transfer o) {
        return 0;
    }

    @Override
    public void init() {
    }    
}
