package com.rizkyagungp.clicpay.dto.client;

import com.rizkyagungp.clicpay.utils.AbstractEntities;

public class ErrorMessage extends AbstractEntities<ErrorMessage> {

    private String indonesian;
    private String english;

    public String getIndonesian() {
        return this.indonesian;
    }

    public void setIndonesian(String indonesian) {
        this.indonesian = indonesian;
    }

    public String getEnglish() {
        return this.english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    @Override
    public int compareTo(ErrorMessage o) {
        return 0;
    }

    @Override
    public void init() {
    }
    
}
