package com.rizkyagungp.clicpay.dto.client;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ReqPaymentOverbooking extends AbstractClient {

    private String requestTime;
    private String sourceAccountNumber;
    private String beneficiaryAccountNumber;
    private String beneficiaryName;
    private String amount;
    private String description;
    private String transactionDate; 

    public String getRequestTime() {
        return this.requestTime;
    }

    public void setRequestTime(String requestTime) {
        this.requestTime = requestTime;
    }

    public String getSourceAccountNumber() {
        return this.sourceAccountNumber;
    }

    public void setSourceAccountNumber(String sourceAccountNumber) {
        this.sourceAccountNumber = sourceAccountNumber;
    }

    public String getBeneficiaryAccountNumber() {
        return this.beneficiaryAccountNumber;
    }

    public void setBeneficiaryAccountNumber(String beneficiaryAccountNumber) {
        this.beneficiaryAccountNumber = beneficiaryAccountNumber;
    }

    public String getBeneficiaryName() {
        return this.beneficiaryName;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    public String getAmount() {
        return this.amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTransactionDate() {
        return this.transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    @Override
    public int compareTo(AbstractClient o) {
        return 0;
    }

    @Override
    public void init() {
        Date now = Calendar.getInstance().getTime();
        this.requestTime = new SimpleDateFormat("yyyyMMddHHmmss").format(now);
    }
    
}
