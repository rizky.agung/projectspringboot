package com.rizkyagungp.clicpay.dto;

import com.rizkyagungp.clicpay.utils.AbstractEntities;

public abstract class AbstractRequest extends AbstractEntities<AbstractRequest> {
    
    private String reffNumber;

    public String getReffNumber() {
        return this.reffNumber;
    }

    public void setReffNumber(String reffNumber) {
        this.reffNumber = reffNumber;
    }

}
