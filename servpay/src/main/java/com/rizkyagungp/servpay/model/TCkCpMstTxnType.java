package com.rizkyagungp.servpay.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.rizkyagungp.servpay.utils.AbstractEntities;

@Entity
@Table(name = "T_CK_CP_MST_TXN_TYPE")
public class TCkCpMstTxnType extends AbstractEntities<TCkCpMstTxnType> {

    private String txtyId;
	private Short txtySeq;
	private String txtyName;
	private String txtyDesc;
	private String txtyDescOth;
	private Character txtyStatus;
	private Date txtyDtCreate;
	private String txtyUidCreate;
	private Date txtyDtLupd;
	private String txtyUidLupd;

    public TCkCpMstTxnType() {
    }

    public TCkCpMstTxnType(String txtyId, Short txtySeq, String txtyName, String txtyDesc, String txtyDescOth, Character txtyStatus, Date txtyDtCreate, String txtyUidCreate, Date txtyDtLupd, String txtyUidLupd) {
        this.txtyId = txtyId;
        this.txtySeq = txtySeq;
        this.txtyName = txtyName;
        this.txtyDesc = txtyDesc;
        this.txtyDescOth = txtyDescOth;
        this.txtyStatus = txtyStatus;
        this.txtyDtCreate = txtyDtCreate;
        this.txtyUidCreate = txtyUidCreate;
        this.txtyDtLupd = txtyDtLupd;
        this.txtyUidLupd = txtyUidLupd;
    }

    @Id
	@Column(name = "TXTY_ID", unique = true, nullable = false, length = 35)
    public String getTxtyId() {
        return this.txtyId;
    }

    public void setTxtyId(String txtyId) {
        this.txtyId = txtyId;
    }

    @Column(name = "TXTY_SEQ")
    public Short getTxtySeq() {
        return this.txtySeq;
    }

    public void setTxtySeq(Short txtySeq) {
        this.txtySeq = txtySeq;
    }

    @Column(name = "TXTY_NAME", nullable = false, length = 100)
    public String getTxtyName() {
        return this.txtyName;
    }

    public void setTxtyName(String txtyName) {
        this.txtyName = txtyName;
    }

    @Column(name = "TXTY_DESC")
    public String getTxtyDesc() {
        return this.txtyDesc;
    }

    public void setTxtyDesc(String txtyDesc) {
        this.txtyDesc = txtyDesc;
    }

    @Column(name = "TXTY_DESC_OTH", length = 512)
    public String getTxtyDescOth() {
        return this.txtyDescOth;
    }

    public void setTxtyDescOth(String txtyDescOth) {
        this.txtyDescOth = txtyDescOth;
    }

    @Column(name = "TXTY_STATUS", nullable = false, length = 1)
    public Character getTxtyStatus() {
        return this.txtyStatus;
    }

    public void setTxtyStatus(Character txtyStatus) {
        this.txtyStatus = txtyStatus;
    }

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TXTY_DT_CREATE", length = 19)
    public Date getTxtyDtCreate() {
        return this.txtyDtCreate;
    }

    public void setTxtyDtCreate(Date txtyDtCreate) {
        this.txtyDtCreate = txtyDtCreate;
    }

    @Column(name = "TXTY_UID_CREATE", length = 35)
    public String getTxtyUidCreate() {
        return this.txtyUidCreate;
    }

    public void setTxtyUidCreate(String txtyUidCreate) {
        this.txtyUidCreate = txtyUidCreate;
    }

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TXTY_DT_LUPD", length = 19)
    public Date getTxtyDtLupd() {
        return this.txtyDtLupd;
    }

    public void setTxtyDtLupd(Date txtyDtLupd) {
        this.txtyDtLupd = txtyDtLupd;
    }

    @Column(name = "TXTY_UID_LUPD", length = 35)
    public String getTxtyUidLupd() {
        return this.txtyUidLupd;
    }

    public void setTxtyUidLupd(String txtyUidLupd) {
        this.txtyUidLupd = txtyUidLupd;
    }

    @Override
    public int compareTo(TCkCpMstTxnType o) {
        return 0;
    }

    @Override
    public void init() {
    }
    
}
