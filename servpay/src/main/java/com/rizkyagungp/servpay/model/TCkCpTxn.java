package com.rizkyagungp.servpay.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.rizkyagungp.servpay.utils.AbstractEntities;

@Entity
@Table(name = "T_CK_CP_TXN")
public class TCkCpTxn  extends AbstractEntities<TCkCpTxn> {

    private String txnId;
    private TCkCpMstTxnState TCkCpMstTxnState;
	private TCkCpMstTxnType TCkCpMstTxnType;
    private String txnReference;
    private Date txnDtReq;
    private String txnReq;
    private String txnParam;
    private String txnResp;
	private Short txnRespCode;
	private Date txnDtResp;
	private Character txnStatus;
	private Date txnDtCreate;
	private String txnUidCreate;
	private Date txnDtLupd;
	private String txnUidLupd;

    public TCkCpTxn() {
    }

    public TCkCpTxn(String txnId, TCkCpMstTxnState TCkCpMstTxnState, TCkCpMstTxnType TCkCpMstTxnType, String txnReference, Date txnDtReq, String txnReq, String txnParam, String txnResp, Short txnRespCode, Date txnDtResp, Character txnStatus, Date txnDtCreate, String txnUidCreate, Date txnDtLupd, String txnUidLupd) {
        this.txnId = txnId;
        this.TCkCpMstTxnState = TCkCpMstTxnState;
        this.TCkCpMstTxnType = TCkCpMstTxnType;
        this.txnReference = txnReference;
        this.txnDtReq = txnDtReq;
        this.txnReq = txnReq;
        this.txnParam = txnParam;
        this.txnResp = txnResp;
        this.txnRespCode = txnRespCode;
        this.txnDtResp = txnDtResp;
        this.txnStatus = txnStatus;
        this.txnDtCreate = txnDtCreate;
        this.txnUidCreate = txnUidCreate;
        this.txnDtLupd = txnDtLupd;
        this.txnUidLupd = txnUidLupd;
    }

    @Id
	@Column(name = "TXN_ID", unique = true, nullable = false, length = 35)
    public String getTxnId() {
        return this.txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TXN_STATE", nullable = false)
    public TCkCpMstTxnState getTCkCpMstTxnState() {
        return this.TCkCpMstTxnState;
    }

    public void setTCkCpMstTxnState(TCkCpMstTxnState TCkCpMstTxnState) {
        this.TCkCpMstTxnState = TCkCpMstTxnState;
    }

    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TXN_TYPE", nullable = false)
    public TCkCpMstTxnType getTCkCpMstTxnType() {
        return this.TCkCpMstTxnType;
    }

    public void setTCkCpMstTxnType(TCkCpMstTxnType TCkCpMstTxnType) {
        this.TCkCpMstTxnType = TCkCpMstTxnType;
    }

    @Column(name = "TXN_REFERENCE", length = 100)
    public String getTxnReference() {
        return this.txnReference;
    }

    public void setTxnReference(String txnReference) {
        this.txnReference = txnReference;
    }

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TXN_DT_REQ", nullable = false, length = 19)
    public Date getTxnDtReq() {
        return this.txnDtReq;
    }

    public void setTxnDtReq(Date txnDtReq) {
        this.txnDtReq = txnDtReq;
    }

    @Column(name = "TXN_REQ", nullable = false, length = 16777215)
    public String getTxnReq() {
        return this.txnReq;
    }

    public void setTxnReq(String txnReq) {
        this.txnReq = txnReq;
    }

    @Column(name = "TXN_PARAM", nullable = false, length = 16777215)
    public String getTxnParam() {
        return this.txnParam;
    }

    public void setTxnParam(String txnParam) {
        this.txnParam = txnParam;
    }

    @Column(name = "TXN_RESP", length = 16777215)
    public String getTxnResp() {
        return this.txnResp;
    }

    public void setTxnResp(String txnResp) {
        this.txnResp = txnResp;
    }

    @Column(name = "TXN_RESP_CODE")
    public Short getTxnRespCode() {
        return this.txnRespCode;
    }

    public void setTxnRespCode(Short txnRespCode) {
        this.txnRespCode = txnRespCode;
    }

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TXN_DT_RESP", length = 19)
    public Date getTxnDtResp() {
        return this.txnDtResp;
    }

    public void setTxnDtResp(Date txnDtResp) {
        this.txnDtResp = txnDtResp;
    }

    @Column(name = "TXN_STATUS", nullable = false, length = 1)
    public Character getTxnStatus() {
        return this.txnStatus;
    }

    public void setTxnStatus(Character txnStatus) {
        this.txnStatus = txnStatus;
    }

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TXN_DT_CREATE", length = 19)
    public Date getTxnDtCreate() {
        return this.txnDtCreate;
    }

    public void setTxnDtCreate(Date txnDtCreate) {
        this.txnDtCreate = txnDtCreate;
    }

    @Column(name = "TXN_UID_CREATE", length = 35)
    public String getTxnUidCreate() {
        return this.txnUidCreate;
    }

    public void setTxnUidCreate(String txnUidCreate) {
        this.txnUidCreate = txnUidCreate;
    }

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TXN_DT_LUPD", length = 19)
    public Date getTxnDtLupd() {
        return this.txnDtLupd;
    }

    public void setTxnDtLupd(Date txnDtLupd) {
        this.txnDtLupd = txnDtLupd;
    }

    @Column(name = "TXN_UID_LUPD", length = 35)
    public String getTxnUidLupd() {
        return this.txnUidLupd;
    }

    public void setTxnUidLupd(String txnUidLupd) {
        this.txnUidLupd = txnUidLupd;
    }

    @Override
    public int compareTo(TCkCpTxn o) {
        return 0;
    }

    @Override
    public void init() {
    }
    
}
