package com.rizkyagungp.servpay.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.rizkyagungp.servpay.utils.AbstractEntities;

@Entity
@Table(name = "T_CK_CP_MST_TXN_STATE")
public class TCkCpMstTxnState extends AbstractEntities<TCkCpMstTxnState>{
    
    private String txstId;
	private Short txstSeq;
	private String txstName;
	private String txstDesc;
	private String txstDescOth;
	private Character txstStatus;
	private Date txstDtCreate;
	private String txstUidCreate;
	private Date txstDtLupd;
	private String txstUidLupd;

    public TCkCpMstTxnState() {
    }

    public TCkCpMstTxnState(String txstId, Short txstSeq, String txstName, String txstDesc, String txstDescOth, Character txstStatus, Date txstDtCreate, String txstUidCreate, Date txstDtLupd, String txstUidLupd) {
        this.txstId = txstId;
        this.txstSeq = txstSeq;
        this.txstName = txstName;
        this.txstDesc = txstDesc;
        this.txstDescOth = txstDescOth;
        this.txstStatus = txstStatus;
        this.txstDtCreate = txstDtCreate;
        this.txstUidCreate = txstUidCreate;
        this.txstDtLupd = txstDtLupd;
        this.txstUidLupd = txstUidLupd;
    }

    @Id
	@Column(name = "TXST_ID", unique = true, nullable = false, length = 35)
    public String getTxstId() {
        return this.txstId;
    }

    public void setTxstId(String txstId) {
        this.txstId = txstId;
    }

    @Column(name = "TXST_SEQ")
    public Short getTxstSeq() {
        return this.txstSeq;
    }

    public void setTxstSeq(Short txstSeq) {
        this.txstSeq = txstSeq;
    }

    @Column(name = "TXST_NAME", nullable = false, length = 100)
    public String getTxstName() {
        return this.txstName;
    }

    public void setTxstName(String txstName) {
        this.txstName = txstName;
    }

    @Column(name = "TXST_DESC")
    public String getTxstDesc() {
        return this.txstDesc;
    }

    public void setTxstDesc(String txstDesc) {
        this.txstDesc = txstDesc;
    }

    @Column(name = "TXST_DESC_OTH", length = 512)
    public String getTxstDescOth() {
        return this.txstDescOth;
    }

    public void setTxstDescOth(String txstDescOth) {
        this.txstDescOth = txstDescOth;
    }

    @Column(name = "TXST_STATUS", nullable = false, length = 1)
    public Character getTxstStatus() {
        return this.txstStatus;
    }

    public void setTxstStatus(Character txstStatus) {
        this.txstStatus = txstStatus;
    }

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TXST_DT_CREATE", length = 19)
    public Date getTxstDtCreate() {
        return this.txstDtCreate;
    }

    public void setTxstDtCreate(Date txstDtCreate) {
        this.txstDtCreate = txstDtCreate;
    }

    @Column(name = "TXST_UID_CREATE", length = 35)
    public String getTxstUidCreate() {
        return this.txstUidCreate;
    }

    public void setTxstUidCreate(String txstUidCreate) {
        this.txstUidCreate = txstUidCreate;
    }

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TXST_DT_LUPD", length = 19)
    public Date getTxstDtLupd() {
        return this.txstDtLupd;
    }

    public void setTxstDtLupd(Date txstDtLupd) {
        this.txstDtLupd = txstDtLupd;
    }

    @Column(name = "TXST_UID_LUPD", length = 35)
    public String getTxstUidLupd() {
        return this.txstUidLupd;
    }

    public void setTxstUidLupd(String txstUidLupd) {
        this.txstUidLupd = txstUidLupd;
    }

    @Override
    public int compareTo(TCkCpMstTxnState o) {
        return 0;
    }

    @Override
    public void init() {
    }
}
