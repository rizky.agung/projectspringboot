package com.rizkyagungp.servpay.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rizkyagungp.servpay.dto.ReqInquiryOverbooking;
import com.rizkyagungp.servpay.dto.ReqPaymentOverbooking;
import com.rizkyagungp.servpay.dto.ResInquiryOverbooking;
import com.rizkyagungp.servpay.dto.ResPaymentOverbooking;
import com.rizkyagungp.servpay.service.ServerService;

import lombok.extern.slf4j.Slf4j;

@RequestMapping(value = "/api/v1/server")
@CrossOrigin
@RestController
@Slf4j
public class ServerController {

    @Autowired
    private ServerService serverService;
    
    @RequestMapping(value = "overbooking/inquiry", method = RequestMethod.POST)
    public ResponseEntity<Object> overbookingInquiry(@RequestBody ReqInquiryOverbooking request, Map<String, String> headers) {
        log.debug("overbookingInquiry");
        ResInquiryOverbooking response = serverService.inquiryOverbooking(request);
        if (null == response.getErrorCode()) {
            return new ResponseEntity<Object>(response, HttpStatus.OK);
        } else 
            return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
    }
    
    @RequestMapping(value = "overbooking/payment", method = RequestMethod.POST)
    public ResponseEntity<Object> overbookingPayment(@RequestBody ReqPaymentOverbooking request, Map<String, String> headers) {
        log.debug("overbookingInquiry");
        ResPaymentOverbooking response = serverService.paymentOverbooking(request);
        if (null == response.getErrorCode()) {
            return new ResponseEntity<Object>(response, HttpStatus.OK);
        } else 
            return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
    }
}
