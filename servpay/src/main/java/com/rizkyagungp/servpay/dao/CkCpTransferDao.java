package com.rizkyagungp.servpay.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rizkyagungp.servpay.model.TCkCpTransfer;

@Repository
public interface CkCpTransferDao extends JpaRepository<TCkCpTransfer, String>{
    
    Optional<TCkCpTransfer> findByTfrBeneficiaryAccountNumberAndTfrStatus(String beneficiaryAccn, Character status);
}
