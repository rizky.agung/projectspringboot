package com.rizkyagungp.servpay.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rizkyagungp.servpay.model.TCkCpTxn;

@Repository
public interface CkCpTxnDao extends JpaRepository<TCkCpTxn, String> {
    
}
