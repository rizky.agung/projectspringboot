package com.rizkyagungp.servpay.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rizkyagungp.servpay.model.TCkCpMstTxnState;

@Repository
public interface CkCpMstTxnStateDao extends JpaRepository<TCkCpMstTxnState, String> {
    
}
