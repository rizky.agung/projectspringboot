package com.rizkyagungp.servpay.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rizkyagungp.servpay.model.TCkCpMstTxnType;

@Repository
public interface CkCpMstTxnTypeDao extends JpaRepository<TCkCpMstTxnType, String> {
    
}
