package com.rizkyagungp.servpay.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rizkyagungp.servpay.constant.Constant;
import com.rizkyagungp.servpay.dao.CkCpTransferDao;
import com.rizkyagungp.servpay.dto.ErrorMessage;
import com.rizkyagungp.servpay.dto.ReqInquiryOverbooking;
import com.rizkyagungp.servpay.dto.ReqPaymentOverbooking;
import com.rizkyagungp.servpay.dto.ResInquiryOverbooking;
import com.rizkyagungp.servpay.dto.ResPaymentOverbooking;
import com.rizkyagungp.servpay.model.TCkCpTransfer;
import com.rizkyagungp.servpay.service.ServerService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ServerServiceImpl implements ServerService {

    @Autowired
    private CkCpTransferDao ckCpTransferDao;

    @Override
    public ResInquiryOverbooking inquiryOverbooking(ReqInquiryOverbooking request) {
        log.debug("inquiryOverbooking");
        ResInquiryOverbooking response = new ResInquiryOverbooking();
        try {
            List<TCkCpTransfer> all = ckCpTransferDao.findAll();
            Optional<TCkCpTransfer> transfer = ckCpTransferDao.findByTfrBeneficiaryAccountNumberAndTfrStatus(request.getAccountNumber(), Constant.QUEUE_STATUS);
            if (!transfer.isPresent())
                throw new Exception("Account not found");

            String refNumber = String.format("%s%s", "9100", String.valueOf(System.currentTimeMillis()));

            response.setUserReferenceNumber(refNumber);
            response.setAccountNumber(request.getAccountNumber());
            response.setAccountName("PT EMULATOR CLICPAY");
            response.setCodeStatus("API000");
            response.setDescriptionStatus("Success");
        } catch (Exception e) {
            response.setErrorCode("API001");
            ErrorMessage err = new ErrorMessage();
            err.setIndonesian("Akun tidak ditemukan");
            err.setEnglish("Account not found");
            response.setErrorMessage(err);
        }
        return response;
    }

    @Override
    public ResPaymentOverbooking paymentOverbooking(ReqPaymentOverbooking request) {
        log.debug("paymentOverbooking");
        ResPaymentOverbooking response = new ResPaymentOverbooking();
        try {
            Optional<TCkCpTransfer> transfer = ckCpTransferDao.findByTfrBeneficiaryAccountNumberAndTfrStatus(request.getBeneficiaryAccountNumber(), Constant.INQUIRY_STATUS);
            if (!transfer.isPresent())
                throw new Exception("Account not found");

            response.setCodeStatus("API000");
            response.setDescriptionStatus("Success");
        } catch (Exception e) {
            response.setCodeStatus("API0001");
            response.setDescriptionStatus("Failed");
        }
        return response;
    }
    
}
