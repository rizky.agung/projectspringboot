package com.rizkyagungp.servpay.service;

import com.rizkyagungp.servpay.dto.ReqInquiryOverbooking;
import com.rizkyagungp.servpay.dto.ReqPaymentOverbooking;
import com.rizkyagungp.servpay.dto.ResInquiryOverbooking;
import com.rizkyagungp.servpay.dto.ResPaymentOverbooking;

public interface ServerService {
    
    ResInquiryOverbooking inquiryOverbooking(ReqInquiryOverbooking request);
    
    ResPaymentOverbooking paymentOverbooking(ReqPaymentOverbooking request);
}
