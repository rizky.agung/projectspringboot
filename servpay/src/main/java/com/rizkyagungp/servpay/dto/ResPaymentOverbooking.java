package com.rizkyagungp.servpay.dto;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ResPaymentOverbooking extends AbstractClient {

    private String responseTime;
    private String codeStatus;
    private String descriptionStatus;

    public String getResponseTime() {
        return this.responseTime;
    }

    public void setResponseTime(String responseTime) {
        this.responseTime = responseTime;
    }

    public String getCodeStatus() {
        return this.codeStatus;
    }

    public void setCodeStatus(String codeStatus) {
        this.codeStatus = codeStatus;
    }

    public String getDescriptionStatus() {
        return this.descriptionStatus;
    }

    public void setDescriptionStatus(String descriptionStatus) {
        this.descriptionStatus = descriptionStatus;
    }

    @Override
    public int compareTo(AbstractClient o) {
        return 0;
    }

    @Override
    public void init() {
        Date now = Calendar.getInstance().getTime();
        this.responseTime = new SimpleDateFormat("yyyyMMddHHmmss").format(now);
    }
    
}
