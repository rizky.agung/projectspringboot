package com.rizkyagungp.servpay.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.rizkyagungp.servpay.utils.AbstractEntities;

@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class AbstractClient extends AbstractEntities<AbstractClient> {
    
    private String userReferenceNumber;

    private String errorCode;
    private ErrorMessage errorMessage;
    private String rrorDescription;

    public String getUserReferenceNumber() {
        return this.userReferenceNumber;
    }

    public void setUserReferenceNumber(String userReferenceNumber) {
        this.userReferenceNumber = userReferenceNumber;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public ErrorMessage getErrorMessage() {
        return this.errorMessage;
    }

    public void setErrorMessage(ErrorMessage errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getRrorDescription() {
        return this.rrorDescription;
    }

    public void setRrorDescription(String rrorDescription) {
        this.rrorDescription = rrorDescription;
    }
}
