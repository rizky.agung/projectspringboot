package com.rizkyagungp.servpay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServpayApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServpayApplication.class, args);
	}

}
