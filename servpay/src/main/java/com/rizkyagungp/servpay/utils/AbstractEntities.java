package com.rizkyagungp.servpay.utils;

import java.io.Serializable;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public abstract class AbstractEntities<T> implements Serializable, Cloneable, Comparable<T> {
 
    public abstract void init();

    public AbstractEntities() {
        init();
    }

    public String toString() {
        try {
            return toJson();
        } catch (Exception e) {
            return e.getMessage();
        } 
    }

    public String toJson() throws Exception {
        try {
            ObjectMapper objMapper = new ObjectMapper();
            objMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
            return objMapper.writeValueAsString(this);
        } catch (Exception e) {
            throw e;
        } 
    }
}
