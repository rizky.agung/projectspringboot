-- MySQL dump 10.13  Distrib 5.7.34, for osx10.18 (x86_64)
--
-- Host: localhost    Database: clicpay
-- ------------------------------------------------------
-- Server version	8.0.33

DROP DATABASE IF EXISTS `clicpay`;

CREATE DATABASE `clicpay`;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `T_CK_CP_MST_TXN_STATE`
--

DROP TABLE IF EXISTS `T_CK_CP_MST_TXN_STATE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_CK_CP_MST_TXN_STATE` (
  `TXST_ID` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'TRANSACTION STATE ID',
  `TXST_SEQ` smallint DEFAULT NULL COMMENT 'SEQUENCE OF THE RECORD FOR DISPLAY PURPOSE',
  `TXST_NAME` varchar(100) NOT NULL COMMENT 'TRANSACTION STATE NAME',
  `TXST_DESC` varchar(255) DEFAULT NULL COMMENT 'TRANSACTION STATE DESCRIPTION',
  `TXST_DESC_OTH` varchar(512) DEFAULT NULL COMMENT 'TRANSACTION STATE DESCRIPTION OTHE LANGUAGE',
  `TXST_STATUS` char(1) NOT NULL COMMENT 'TRANSACTION STATE STATUS',
  `TXST_DT_CREATE` datetime DEFAULT NULL COMMENT 'RECORD CREATED DATE',
  `TXST_UID_CREATE` varchar(35) DEFAULT NULL COMMENT 'RECORD CREATED USER',
  `TXST_DT_LUPD` datetime DEFAULT NULL COMMENT 'RECORD LAST UPDATE DATE',
  `TXST_UID_LUPD` varchar(35) DEFAULT NULL COMMENT 'RECORD LAST UPDATE USER',
  PRIMARY KEY (`TXST_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='MASTER TRANSACTION STATE';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_CK_CP_MST_TXN_STATE`
--

LOCK TABLES `T_CK_CP_MST_TXN_STATE` WRITE;
/*!40000 ALTER TABLE `T_CK_CP_MST_TXN_STATE` DISABLE KEYS */;
INSERT INTO `T_CK_CP_MST_TXN_STATE` VALUES ('EXCEPTION',4,'EXCEPTION','EXCEPTION','EXCEPTION','A','2023-10-20 14:49:10','SYS','2023-10-20 21:49:09','SYS'),('FAILED',3,'FAILED','FAILED','FAILED','A','2023-10-20 14:49:10','SYS','2023-10-20 21:49:09','SYS'),('NEW',1,'NEW','NEW','NEW','A','2023-10-20 14:49:10','SYS','2023-10-20 21:49:09','SYS'),('SUCCESS',2,'SUCCESS','SUCCESS','SUCCESS','A','2023-10-20 14:49:10','SYS','2023-10-20 21:49:09','SYS');
/*!40000 ALTER TABLE `T_CK_CP_MST_TXN_STATE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_CK_CP_MST_TXN_TYPE`
--

DROP TABLE IF EXISTS `T_CK_CP_MST_TXN_TYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_CK_CP_MST_TXN_TYPE` (
  `TXTY_ID` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'TRANSACTION TYPE ID',
  `TXTY_SEQ` smallint DEFAULT NULL COMMENT 'SEQUENCE OF THE RECORD FOR DISPLAY PURPOSE',
  `TXTY_NAME` varchar(100) NOT NULL COMMENT 'TRANSACTION TYPE NAME',
  `TXTY_DESC` varchar(255) DEFAULT NULL COMMENT 'TRANSACTION TYPE DESCRIPTION',
  `TXTY_DESC_OTH` varchar(512) DEFAULT NULL COMMENT 'TRANSACTION TYPE DESCRIPTION OTHE LANGUAGE',
  `TXTY_STATUS` char(1) NOT NULL COMMENT 'TRANSACTION TYPE STATUS',
  `TXTY_DT_CREATE` datetime DEFAULT NULL COMMENT 'RECORD CREATED DATE',
  `TXTY_UID_CREATE` varchar(35) DEFAULT NULL COMMENT 'RECORD CREATED USER',
  `TXTY_DT_LUPD` datetime DEFAULT NULL COMMENT 'RECORD LAST UPDATE DATE',
  `TXTY_UID_LUPD` varchar(35) DEFAULT NULL COMMENT 'RECORD LAST UPDATE USER',
  PRIMARY KEY (`TXTY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='MASTER TRANSACTION TYPE';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_CK_CP_MST_TXN_TYPE`
--

LOCK TABLES `T_CK_CP_MST_TXN_TYPE` WRITE;
/*!40000 ALTER TABLE `T_CK_CP_MST_TXN_TYPE` DISABLE KEYS */;
INSERT INTO `T_CK_CP_MST_TXN_TYPE` VALUES ('TRANSFER',1,'TRANSFER','TRANSFER','TRANSFER','A','2023-10-20 15:16:36','SYS','2023-10-20 22:16:35','SYS');
/*!40000 ALTER TABLE `T_CK_CP_MST_TXN_TYPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_CK_CP_TRANSFER`
--

DROP TABLE IF EXISTS `T_CK_CP_TRANSFER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_CK_CP_TRANSFER` (
  `TFR_ID` varchar(35) NOT NULL COMMENT 'OVERBOOKING ID',
  `TFR_TXN_ID` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'TRANSACTION ID - REFERENCE - T_CK_CP_TXN:TXN_ID',
  `TFR_TYPE_ID` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'TRANSACTION TYPE ID - REFERENCE T_CK_CP_MST_TXN_TYPE:TXTY_ID',
  `TFR_REFERENCE` varchar(100) DEFAULT NULL COMMENT 'TRANSFER REFERENCE',
  `TFR_SENDER_ACCOUNT_NUMBER` varchar(35) DEFAULT NULL COMMENT 'SENDER ACCOUNT NUMBER',
  `TFR_SENDER_ACCOUNT_NAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'SENDER ACCOUNT NAME',
  `TFR_BENEFICIARY_BANK` varchar(35) DEFAULT NULL COMMENT 'BENEFICIARY BANK ACCOUNT',
  `TFR_BENEFICIARY_ACCOUNT_NUMBER` varchar(35) DEFAULT NULL COMMENT 'BENEFICIARY ACCOUNT NUMBER',
  `TFR_BENEFICIARY_ACCOUNT_NAME` varchar(255) DEFAULT NULL COMMENT 'BENEFICIARY ACCOUNT NAME',
  `TFR_BILL_AMOUNT` decimal(15,2) DEFAULT NULL COMMENT 'TRANSACTION AMOUNT',
  `TFR_PAY_AMOUNT` decimal(15,2) DEFAULT NULL COMMENT 'PAY AMOUNT',
  `TFR_CCY` varchar(35) DEFAULT NULL COMMENT 'CURRENCY',
  `TFR_PAYMENT_REFERENCE` varchar(255) DEFAULT NULL COMMENT 'PAYMENT REFERENCE',
  `TFR_DESCRIPTION` varchar(512) DEFAULT NULL COMMENT 'PAYMENT DESCRIPTION',
  `TFR_STATUS` char(1) NOT NULL COMMENT 'PAYMENT STATUS',
  `TFR_DT_CREATE` datetime DEFAULT NULL COMMENT 'RECORD CREATED DATE',
  `TFR_UID_CREATE` varchar(35) DEFAULT NULL COMMENT 'RECORD CREATED USER',
  `TFR_DT_LUPD` datetime DEFAULT NULL COMMENT 'RECORD LAST UPDATED DATE',
  `TFR_UID_LUPD` varchar(35) DEFAULT NULL COMMENT 'RECORD LAST UPDATED USER'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='TRANSFER TRANSACTION';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `T_CK_CP_TXN`
--

DROP TABLE IF EXISTS `T_CK_CP_TXN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_CK_CP_TXN` (
  `TXN_ID` varchar(35) NOT NULL COMMENT 'TRANSACTION ID',
  `TXN_TYPE` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'TRANSACTION TYPE - REFERENCE T_PAY_TYPE:TXTY_ID',
  `TXN_STATE` varchar(35) NOT NULL COMMENT 'TRANSACTION STATE - REFERENCE T_PAY_TRANSACTION_STATE:TXST_ID',
  `TXN_REFERENCE` varchar(100) DEFAULT NULL COMMENT 'TRANSACTION REFERENCE',
  `TXN_DT_REQ` datetime NOT NULL COMMENT 'TRANSACTION DATE TIME REQUEST',
  `TXN_REQ` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'TRANSACTION REQUEST URL',
  `TXN_PARAM` mediumtext NOT NULL COMMENT 'TRANSACTION REQUEST BODY',
  `TXN_RESP` mediumtext COMMENT 'TRANSACTION RESPONSE',
  `TXN_RESP_CODE` smallint DEFAULT NULL COMMENT 'TRANSACTION RESPONSE CODE',
  `TXN_DT_RESP` datetime DEFAULT NULL COMMENT 'TRANSACTION DATE TIME RESPONSE',
  `TXN_STATUS` char(1) NOT NULL COMMENT 'TRANSACTION STATUS',
  `TXN_DT_CREATE` datetime DEFAULT NULL COMMENT 'RECORD CREATED DATE',
  `TXN_UID_CREATE` varchar(35) DEFAULT NULL COMMENT 'RECORD CREATED USER',
  `TXN_DT_LUPD` datetime DEFAULT NULL COMMENT 'RECORD LAST UPDATE DATE',
  `TXN_UID_LUPD` varchar(35) DEFAULT NULL COMMENT 'RECORD LAST UPDATE USER',
  PRIMARY KEY (`TXN_ID`),
  KEY `FK_PAY_TRANSACTION_PAY_TRANSACTION_STATE` (`TXN_STATE`),
  KEY `FK_CK_CP_TXN_CK_CP_TXN_TYPE` (`TXN_TYPE`),
  CONSTRAINT `FK_CK_CP_TXN_CK_CP_TXN_STATE` FOREIGN KEY (`TXN_STATE`) REFERENCES `T_CK_CP_MST_TXN_STATE` (`TXST_ID`),
  CONSTRAINT `FK_CK_CP_TXN_CK_CP_TXN_TYPE` FOREIGN KEY (`TXN_TYPE`) REFERENCES `T_CK_CP_MST_TXN_TYPE` (`TXTY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='LOG TRANSACTION';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_CK_CP_TXN`
--

LOCK TABLES `T_CK_CP_TXN` WRITE;
/*!40000 ALTER TABLE `T_CK_CP_TXN` DISABLE KEYS */;
INSERT INTO `T_CK_CP_TXN` VALUES ('231021231573316','TRANSFER','NEW','TXN2023102114045','2023-10-21 23:15:25','/api/v1/clicpay/transfer/overbooking','{\"reffNumber\":\"TXN2023102114045\",\"senderAccount\":\"12345678910\",\"beneficiaryAccount\":\"910009876543211\",\"amount\":15000.00,\"ccy\":\"IDR\"}','{\"responseTime\":\"20231021231524\",\"codeStatus\":\"API000\",\"descriptionStatus\":\"Success\"}',200,'2023-10-21 23:15:25','A','2023-10-21 23:15:25','SYS','2023-10-21 23:15:25','SYS');
/*!40000 ALTER TABLE `T_CK_CP_TXN` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'clicpay'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-10-22 10:32:51
