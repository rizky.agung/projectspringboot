Welcome to sample project **payment**

-- steps for installation --
1. cd clicpay / servpay
2. mvn clean install
3. docker build -t springboot-docker-clicpay:latest .
4. docker build -t springboot-docker-servpay:latest .
5. docker-compose up
